package data

import assertEquals
import org.junit.Test
import readers.BasicTabReader
import java.io.File

class BasicTabReaderTest {
    @Test fun testAll() {
        val notes = BasicTabReader().readNotes(File("notes/ferrumaeternum.notes"))
        val noteNames = notes.map({ "${it.first().noteName}${it.first().octave}" })
        assertEquals(noteNames.take(5), listOf("E3", "E3", "FIS3", "G3", "G3"))
    }
}
