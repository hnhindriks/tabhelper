package data

import assertEquals
import org.junit.Test
import readers.OutputTabReader
import java.io.File

class OutputTabReaderTest{
    @Test
    fun testReader() {
        val notes = OutputTabReader().readNotes(File("notes/ferrumaeternum.tab"))
        val noteNames = notes.map({ "${it.first().noteName}${it.first().octave}" })
        assertEquals(noteNames.take(5), listOf("A4", "A4", "B4", "C4", "C4"))
    }
}

