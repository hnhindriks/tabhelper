package data

import assertEquals
import org.junit.Test

class GuitarStringTest{
    @Test
    fun testStrings(){
        val lowE = GuitarString(Note(NoteName.E, 2))
        assertEquals(lowE.getNoteAtFret(0), Note(NoteName.E, 2))
        assertEquals(lowE.getNoteAtFret(5), Note(NoteName.A, 3))
        assertEquals(lowE.getNoteAtFret(12), Note(NoteName.E, 3))
        assertEquals(lowE.getNoteAtFret(22), Note(NoteName.D, 4))
        assertEquals(lowE.getNoteAtFret(24), Note(NoteName.E, 4))
    }

    @Test
    fun testTunings(){
        //TODO add assertions
        println(getSixStringTuning(Note(NoteName.E, 2)).prettyPrinted())
        println(getSixStringTuning(Note(NoteName.D, 2)).prettyPrinted())
        println(getSixStringTuning(Note(NoteName.C, 2)).prettyPrinted())
        println(getSixStringTuning(Note(NoteName.B, 2)).prettyPrinted())
    }
}

fun List<GuitarString>.prettyPrinted() = this.map { it.openNote.getShortName() }