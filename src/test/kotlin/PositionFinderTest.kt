import data.*
import misc.prettyPrinted
import org.junit.Assert
import org.junit.Test
import search.PlayerState
import search.PositionFinder


class PositionFinderTest{

    //TODO extend test case
    @Test
    fun testFindPositions(){
        val pf  = PositionFinder()
        println(pf.findPositions(setOf(Note(NoteName.E,2)),ESTANDARD))
        val expected = setOf(
                //open strings
                PlayerState(0,mapOf(
                        Pair(ESTANDARD[2],FingerPosition(Finger.OPEN)),
                        Pair(ESTANDARD[3], FingerPosition(Finger.OPEN))
                )),
                //5th position
                PlayerState(5,mapOf(
                        Pair(ESTANDARD[1],FingerPosition(Finger.INDEX,5)),
                        Pair(ESTANDARD[2], FingerPosition(Finger.MIDDLE,5))
                ))
        )
        val actual = pf.findPositions(
                setOf(
                        Note(NoteName.D,3),
                        Note(NoteName.G,3)
                ),ESTANDARD)
        println(expected.toList().prettyPrinted())
        println(actual.toList().prettyPrinted())
        System.out.flush()
        Thread.sleep(1000)
        Assert.assertTrue(actual.containsAll(expected))
    }

    @Test
    fun testFindFretCombinations(){
        val pf  = PositionFinder()
        Assert.assertEquals(setOf(
                mapOf(Note(NoteName.E,2) to StringPosition(GuitarString(Note(NoteName.E,2)),0))
        ),
                pf.findFretCombinations(setOf(Note(NoteName.E,2)),ESTANDARD)
        )
        Assert.assertEquals(setOf(
                mapOf(Note(NoteName.E,3) to StringPosition(GuitarString(Note(NoteName.E,2)),12)),
                mapOf(Note(NoteName.E,3) to StringPosition(GuitarString(Note(NoteName.A,3)),7)),
                mapOf(Note(NoteName.E,3) to StringPosition(GuitarString(Note(NoteName.D,3)),2))
        ),
                pf.findFretCombinations(setOf(Note(NoteName.E,3)),ESTANDARD)
        )
        Assert.assertEquals(6, pf.findFretCombinations(setOf(Note(NoteName.E,4)),ESTANDARD).size)
        Assert.assertEquals(3, pf.findFretCombinations(setOf(Note(NoteName.E,5)),ESTANDARD).size)
    }
}
