import misc.powerSet
import misc.subSets
import org.junit.Test

class UtilsTest {
    @Test
    public fun testPowerset() {
        assertEquals(
                setOf(1, 2, 3).powerSet(),
                setOf(
                        setOf(),
                        setOf(1),
                        setOf(1, 2, 3),
                        setOf(3),
                        setOf(2),
                        setOf(1, 2),
                        setOf(1, 3),
                        setOf(2, 3)
                )
        )
    }

    @Test
    public fun testSubset() {
        assertEquals(
                setOf(1, 2, 3).subSets(2),
                setOf(
                        setOf(1, 3),
                        setOf(1, 2),
                        setOf(2, 3)))
        assertEquals(
                setOf(1, 2, 3, 4).subSets(3),
                setOf(
                        setOf(2, 3, 1),
                        setOf(2, 4, 1),
                        setOf(4, 3, 1),
                        setOf(4, 3, 2)
                )
        )
        assertEquals(
                setOf(1, 2, 3).subSets(3),
                setOf(setOf(2, 3, 1))
        )
    }

    @Test
    public fun benchmark() {
        val input = setOf(1, 2, 3, 4, 5, 6)
        val n = 1000
        var time = System.currentTimeMillis()
        for (i in 0..n - 1) {
            input.powerSet()
        }
        println("Powerset took: ${System.currentTimeMillis() - time}ms")
        time = System.currentTimeMillis()
        for (i in 0..5 * n - 1) {
            input.subSets(2)
        }
        println("Subset took: ${System.currentTimeMillis() - time}ms")
    }
}
