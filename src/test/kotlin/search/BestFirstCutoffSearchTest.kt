package search

import misc.toTabs
import org.junit.Test
import search.strategies.BestFirstCutoffSearch
import search.strategies.CutoffDFSSearch

class BestFirstCutoffSearchTest : SearchTest(){
    override fun algorithm() = BestFirstCutoffSearch(verbose = true)

    override fun postTest(searchAlgorithm : SearchAlgorithm){
        println((searchAlgorithm as BestFirstCutoffSearch).positionFinder.localCache.stats())
    }

    @Test
    fun testMultipleResults(){
        /*runTests { notes, strings, searchAlgorithm ->
            (searchAlgorithm as BestFirstCutoffSearch).
                    generateStrategies(notes,strings).
                    map { it.theSolution.toTabs(strings)+"\n" }.
                    forEach { println(it) }
            true
        }*/
    }
}
