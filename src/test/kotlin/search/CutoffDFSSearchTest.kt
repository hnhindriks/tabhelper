package search

import misc.toTabs
import org.junit.Test
import search.strategies.CutoffDFSSearch

class CutoffDFSSearchTest : SearchTest(){
    override fun algorithm() = CutoffDFSSearch(verbose = true)

    override fun postTest(searchAlgorithm : SearchAlgorithm){
        println((searchAlgorithm as CutoffDFSSearch).positionFinder.localCache.stats())
    }

    @Test
    fun testMultipleResults(){
        /*runTests { notes, strings, searchAlgorithm ->
            (searchAlgorithm as CutoffDFSSearch).generateStrategies(notes,strings).map { it.theSolution.toTabs(strings)+"\n" }.forEach { println(it) }
            true
        }*/
    }
}
