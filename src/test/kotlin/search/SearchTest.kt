package search

import data.ESTANDARD
import data.GuitarString
import data.Note
import misc.prettyPrinted
import misc.toTabs
import org.junit.Test
import readers.BasicTabReader
import java.io.File

abstract class SearchTest {
    protected abstract fun algorithm() : SearchAlgorithm

    protected open fun postTest(searchAlgorithm : SearchAlgorithm){}

    protected fun runTests(runner : (List<Set<Note>>,List<GuitarString>,alg : SearchAlgorithm) -> Boolean){
        for(notesFile in listOf("ferrumaeternum","ropes")) {
            println("Testing: $notesFile")
            val notes = BasicTabReader().readNotes(File("notes/$notesFile.notes"))
            val timestamp = System.currentTimeMillis()
            val alg = algorithm()
            runner(notes, ESTANDARD,alg)
            println("Calculated solution for $notesFile in ${System.currentTimeMillis()-timestamp}ms")
            postTest(alg)
        }
    }

    @Test
    fun testSearchSimple() {
        runTests {
            notes, strings, algorithm ->
            val result = algorithm.generateStrategy(notes.take(6), ESTANDARD)
            result.forEach {
                println(it.score)
                println(it.theSolution.prettyPrinted())
            }
            true
        }
    }

    @Test
    fun testSearch() {
        runTests {
            notes, strings, algorithm ->
            val strategy = algorithm.generateStrategy(notes, ESTANDARD)
            strategy.forEach {
                println(it.score)
                println(it.theSolution.prettyPrinted())
                println(it.theSolution.toTabs(ESTANDARD))
            }
            true
        }
    }
}
