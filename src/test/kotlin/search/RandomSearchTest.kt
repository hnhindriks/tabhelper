package search

import search.strategies.RandomSearch

class RandomSearchTest : SearchTest(){
    override fun algorithm(): SearchAlgorithm = RandomSearch()

}
