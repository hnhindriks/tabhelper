package search

import search.strategies.GreedySearch

class GreedySearchTest : SearchTest(){
    override fun algorithm() : SearchAlgorithm = GreedySearch()
}
