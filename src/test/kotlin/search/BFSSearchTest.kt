package search

import search.strategies.RandomSearch

class BFSSearchTest : SearchTest(){
    //TODO restore test when BFS is fixed
    override fun algorithm() = RandomSearch()
}
