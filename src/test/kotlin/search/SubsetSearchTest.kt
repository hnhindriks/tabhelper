package search

import search.strategies.SubsetSearch

class SubsetSearchTest : SearchTest(){
    override fun algorithm() : SearchAlgorithm = SubsetSearch(verbose = true)
}
