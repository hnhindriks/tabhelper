package search.costs

import search.PlayerState

fun defaultCostFunction() : Cost = LeftHandMovementCost()

interface Cost {
    //TODO create total cost evaluation with cutoff
    fun getTotalCost(states : List<PlayerState>) : Int
    fun getAdditionalCost(states : List<PlayerState>, addition: PlayerState) : Int
    fun getAdditionalCost(a : PlayerState,b : PlayerState) : Int
}

class LeftHandMovementCost : Cost{
    override fun getAdditionalCost(a: PlayerState, b: PlayerState): Int = Math.abs(a.position-b.position)

    override fun getTotalCost(states: List<PlayerState>) =
        states.fold(initial = 0 to -1, operation = { scoreInfo, state ->
            val score = scoreInfo.first
            val position = scoreInfo.second
            if (position == -1) {
                score to state.position
            } else {
                score + Math.abs(position - state.position) to state.position
            }
        }).first

    override fun getAdditionalCost(states: List<PlayerState>, addition: PlayerState) =
            states.findLast { state -> state.position!=0 }?.let { getAdditionalCost(it,addition) } ?: 0
}