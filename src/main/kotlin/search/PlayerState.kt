package search

import data.Finger
import data.FingerPosition
import data.GuitarString

/**
 * Models the state of the virtual guitarplayer
 */
data class PlayerState(val position: Int = 0, val fingering: Map<GuitarString, FingerPosition> = mapOf()) {
    fun slideHand(newPosition: Int): PlayerState {
        require(newPosition >= 0) { "Illegal position: " + newPosition }
        return copy(position = newPosition)
    }

    fun getEssentialString(): String {
        val fingerings = fingering.orEmpty().entries
                .filter({ e -> e.value.finger !== Finger.THUMB })
                .filter({ e -> e.value.finger !== Finger.OPEN })
                .sortedBy({ it.value.finger })
                .map({ fp -> "${fp.key.openNote.getShortName()} ${fp.value.finger}: ${fp.value.position}}" })
                .joinToString(separator = " ")
        return "[$position, $fingerings]"
    }
}