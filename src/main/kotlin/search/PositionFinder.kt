package search

import com.google.common.base.Function
import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import data.*
import misc.maxFret
import misc.subSets
import org.chocosolver.solver.Solver
import org.chocosolver.solver.constraints.IntConstraintFactory
import org.chocosolver.solver.search.solution.AllSolutionsRecorder
import org.chocosolver.solver.variables.VariableFactory

//TODO FEATURE barre support
class PositionFinder{
    val localCache: LoadingCache<Pair<Set<Note>,Collection<GuitarString>>,Set<Map<Note,StringPosition>>> =
            CacheBuilder.newBuilder().
                    maximumSize(1000).
                    recordStats().
                    build(CacheLoader.from(Function { findFretCombinationsUncached(it.first,it.second) }))

    /**
     * Finds all possible positions in which the given note group can be played
     * @param notegroup the notes to be played (expected are single notes or chords)
     * @param strings the strings on which the notes should be played
     *
     * @return a set of playerstate objects, representing all the possible positions including fingerings of the given notes
     */
    fun findPositions(notegroup: Set<Note>, strings: Collection<GuitarString>): Set<PlayerState> {
        require((notegroup.size <= strings.size), { "Can't play 2 notes on a single string at the same time" })
        //we want for each note to select a string on which it can be played
        //in a playable position, every note maps to some string note -> fingering
        //to play chords, we have to create all possible mappings notes[] -> fingering[] with every f.s unique
        return findFretCombinations(notegroup,strings).
                filter { isPlayable(it) }.
                flatMap { generateFingerings(it) }.toSet()
    }

    /**
     * Checks whether the smallest and largest non-open positions are within reach
     *
     * @return whether every position can be reached from the lowest position, excluding open notes
     */
    internal fun isPlayable(it: Map<Note,StringPosition>): Boolean {
        val indices = it.filterValues { it.position!=0 }.values.map { it.position }.sorted()
        val first = indices.firstOrNull()
        val last = indices.lastOrNull()
        return first?.let { f -> last?.let { it < maxFret(f)} } ?: true
    }

    /**
     * Generate all possible fingerings based on the given fret combination
     */
    internal fun generateFingerings(fretCombination : Map<Note,StringPosition>) : Set<PlayerState>{
        val partitionedStrings = fretCombination.toList().partition { it.second.position==0 }
        val openStrings : List<Pair<Note,StringPosition>> = partitionedStrings.first
        val nonOpenStrings : List<Pair<Note,StringPosition>> = partitionedStrings.second
        val availableFingers : Set<Finger> = setOf(Finger.INDEX,Finger.MIDDLE,Finger.RING,Finger.PINKY)


        //only return a result if we have enough fingers to play the notes (without barre)
        if (availableFingers.size < nonOpenStrings.size) {
            return setOf()
        } else if (nonOpenStrings.size == 0){
            return setOf(PlayerState(fingering = playAsOpenStrings(openStrings)))
        }else {
            val resultOpen = playAsOpenStrings(openStrings)

            //TODO assign all fingers if possible
            //TODO fix position, set it to lowest assignable finger position, taking into account the positionOffset
            return (availableFingers.subSets(nonOpenStrings.size).
                    map { fingers ->
                        val fingerpositions = fingers.sorted().
                                mapIndexed { i, finger -> finger to nonOpenStrings[i] }.
                                map {it.second.second.string to FingerPosition(it.first,it.second.second.position)}.
                                toMap() + resultOpen
                        PlayerState(fingering = fingerpositions,
                                    position = (fingerpositions.values.map { it.position-it.finger.positionOffset!! }).min()!!
                        )
                    }
                    ).toSet()
        }
    }

    internal fun playAsOpenStrings(map : List<Pair<Note,StringPosition>>) = map.map {it.second.string to FingerPosition(Finger.OPEN)}.toMap()

    fun findFretCombinations(notegroup: Set<Note>, strings:Collection<GuitarString>) : Set<Map<Note,StringPosition>>{
        return localCache[notegroup to strings]
    }

    internal fun findFretCombinationsUncached(notegroup: Set<Note>, strings:Collection<GuitarString>) : Set<Map<Note,StringPosition>>{
        val solver = Solver("Posfind: $notegroup")

        val stringsIndexed = strings.toList()
        val notevars = notegroup.map { it to VariableFactory.bounded(it.getShortName(),0, stringsIndexed.size -1,solver)}.toMap()

        //no notes on same string
        solver.post(IntConstraintFactory.alldifferent(notevars.values.toTypedArray()))

        //only play notes on strings with that note
        notegroup.forEach { note ->
            solver.post(IntConstraintFactory.member(notevars[note],
                    stringsIndexed.
                            mapIndexed { i, guitarString -> if (guitarString.canPlayNote(note)) i else null }.
                            filterNotNull().toIntArray())
            )
        }

        val solutionsRecorder = AllSolutionsRecorder(solver)
        solver.set(solutionsRecorder)

        solver.findAllSolutions()

        return solutionsRecorder.solutions.map { solution -> notevars.
                map { it.key to solution.getIntVal(it.value) }.
                map { it.first to stringsIndexed[it.second] }.
                map { it.first to StringPosition(it.second,it.second.getPosition(it.first)!!) }.
                toMap()
        }.toSet()
    }
}