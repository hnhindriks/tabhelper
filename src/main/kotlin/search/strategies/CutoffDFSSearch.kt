package search.strategies

import data.GuitarString
import data.Note
import search.PlayerState
import search.PositionFinder
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction
import java.util.*

//Depth-first-search with best-result cut-off, quickly exits bad branches but uses lots of heuristic calculations
data class CutoffDFSSearch(val solutionCount : Int = 5, var solutions : List<Solution> = mutableListOf(), val cost : Cost = defaultCostFunction(), val positionFinder: PositionFinder = PositionFinder(), val verbose : Boolean = false) : SearchAlgorithm {

    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {
        return generateStrategies(notes,strings)
    }

    fun generateStrategies(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {
        solutions = mutableListOf()
        generateStrategyCutoff(notes,strings)
        return solutions
    }

    private fun generateStrategyCutoff(notes: List<Set<Note>>, strings: Collection<GuitarString>, currentSolution : List<PlayerState> = listOf()) : List<PlayerState>?{
        //base case 1 - best solution is better than our solution, and our solution can only get worse
        //base case 2 - all notes have been processed, store current result (which must be better than the best result)
        //recursion actions - foreach notegroup: recur; drop all bad solutions
        val heuristicResult = cost.getTotalCost(currentSolution)
        if(solutions.lastOrNull()?.let { it.score <= heuristicResult } ?: false){
            //base case 1
            return null;
        }else if(notes.size==0){
            //base case 2
            queueSolution(heuristicResult,currentSolution)
            return currentSolution
        }else {
            //recursion
            val positions = positionFinder.findPositions(notes.firstOrNull()!!, strings)
            val solveRecursively = positions.map { nextNotePosition ->
                    //evaluate next
                    val proposedSolution = currentSolution + nextNotePosition
                    generateStrategyCutoff(notes.drop(1), strings, proposedSolution)
            }.filterNotNull() //select valid and better solutions
            val minimizedByCost = solveRecursively.minBy { cost.getTotalCost(it)}
            return minimizedByCost
        }
    }

    private fun queueSolution(heuristicResult : Int,solution : List<PlayerState>){
        if(verbose)println("New best solution: $heuristicResult;")
        solutions += Solution(solution, heuristicResult)
        solutions = solutions.takeLast(solutionCount)
    }
}
