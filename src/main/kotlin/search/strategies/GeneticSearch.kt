package search.strategies

import data.GuitarString
import data.Note
import search.PlayerState
import search.PositionFinder
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction
import java.math.BigInteger
import java.util.*

val selectionProbability = 0.5
val mutationProbability = .2

class GeneticSearch(val cost : Cost = defaultCostFunction(), val positionFinder: PositionFinder = PositionFinder(), val verbose : Boolean = false) : SearchAlgorithm {
    val pandaCount = 10
    val generations = 1000
    var currentBest : Pair<Int,List<PlayerState>>? = null

    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {

        val randomness : Random = Random(42)
        var pandas : List<Panda> = listOf()

        //init the population with pandas
        for(int in 1..pandaCount){
            pandas += Panda(
                    BigInteger.valueOf(
                            Math.abs(randomness.nextInt()).toLong()
                    )
            )
        }

        //GO
        for(generation in 1..generations){
            if(verbose)println("Generated generation $generation, best solution: ${currentBest?.first}")
            val fitPandas = pandas.map { it to it.getFitness(notes,strings,positionFinder,cost) }.sortedBy { it.second }
            //println(fitPandas.map { it.first.gene }.toSet())
            updateCurrentBest(fitPandas.first().second,fitPandas.first().first.solve(notes,strings,positionFinder))

            //generate new pandas
            pandas = listOf()
            for(child in 1..pandaCount){ //kill the old pandas
                var pandaCouple : List<Panda> = listOf()
                for(panda in fitPandas){
                    if(randomness.nextDouble()<selectionProbability){
                        pandaCouple += panda.first
                    }
                    if(pandaCouple.size==2)break
                }
                pandaCouple+=fitPandas.takeLast(2-pandaCouple.size).map { it.first }
                assert(pandaCouple.size==2)
                pandas += pandaCouple.first().mate(pandaCouple.last(),randomness)
            }
        }


        return listOf(Solution(currentBest!!.second,currentBest!!.first))
    }

    private fun updateCurrentBest(score: Int, solution : List<PlayerState>) {
        if(currentBest!=null){
            if(score < currentBest!!.first){
                currentBest = score to solution
            }
        }else{
            currentBest=score to solution
        }
    }

    /**
     * A panda which plays guitar, and reproduces
     */
    data class Panda(val gene: BigInteger){
        fun getFitness(notes: List<Set<Note>>, strings: Collection<GuitarString>, positionFinder : PositionFinder, cost: Cost) : Int {
            return cost.getTotalCost(solve(notes,strings,positionFinder))
        }

        fun solve(notes: List<Set<Note>>, strings: Collection<GuitarString>, positionFinder : PositionFinder) : List<PlayerState>{
            val effectiveGene : Random = Random(gene.longValueExact())
            return notes.fold(listOf<PlayerState>(),{currentSolution, notegroup ->
                currentSolution + positionFinder.findPositions(notegroup,strings).
                        sortedBy { effectiveGene.nextBoolean() }.
                        first()
            })
        }

        fun mate(other : Panda, randomness : Random) : Panda{
            //splice the gene
            var result  = gene

            for(bit in 0..32){
                //equally distribute the choice of bits
                if(randomness.nextBoolean()){
                    if(other.gene.testBit(bit)) {
                        result = result.setBit(bit)
                    } else {
                        result = result.clearBit(bit)
                    }
                }

                //mutate some bits
                if(randomness.nextDouble()<mutationProbability){
                    result = result.flipBit(bit)
                }
            }

            //println("New panda generated, old a: $gene, old b: ${other.gene}, new: $result")

            //generate new panda
            return Panda(result)
        }
    }
}