package search.strategies

import data.GuitarString
import data.Note
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction

/**
 * Concatenates solutions of small parts, and concatenates those
 */
data class SubsetSearch(val costFunction : Cost = defaultCostFunction(), val verbose: Boolean = false) : SearchAlgorithm {
    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {
        return listOf() //FIXME kotlin bug
        /*
        val pf = PositionFinder() //use the same cache for all parts
        var results : List<List<PlayerState>> = listOf();
        for(blocksize in 2..32) {
            if(verbose)println("Blocksize: $blocksize")
            var result: List<PlayerState> = listOf();
            for (i in 0..notes.size - 1 step blocksize) {
                val part = notes.subList(i, Math.min(i + blocksize, notes.size))
                val search = CutoffDFSSearch(cost = costFunction, positionFinder = pf)
                result += search.generateStrategy(part, strings)
            }
            results += result
        }
        if(verbose)println("Results: "+results.map { costFunction.getTotalCost(it) }.joinToString(separator = ","))
        println(pf.localCache.stats())
        return results.minBy { costFunction.getTotalCost(it) }!!
        */
    }
}