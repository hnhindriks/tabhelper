package search.strategies

import data.GuitarString
import data.Note
import search.PlayerState
import search.PositionFinder
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction

//Depth-first-search with best-result cut-off, quickly exits bad branches but uses lots of heuristic calculations
data class BestFirstCutoffSearch(val solutionCount : Int = 5, var solutions : List<Solution> = mutableListOf(), val cost : Cost = defaultCostFunction(), val positionFinder: PositionFinder = PositionFinder(), val verbose : Boolean = false) : SearchAlgorithm {

    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {
        return generateStrategies(notes,strings)
    }

    fun generateStrategies(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {
        solutions = mutableListOf()
        generateStrategyCutoff(notes,strings)
        return solutions
    }

    private fun generateStrategyCutoff(notes: List<Set<Note>>, strings: Collection<GuitarString>, currentSolution : List<PlayerState> = listOf()) : List<PlayerState>?{
        //base case 1 - best solution is better than our solution, and our solution can only get worse
        //base case 2 - all notes have been processed, store current result (which must be better than the best result)
        //recursion actions - foreach notegroup: recur; drop all bad solutions
        val heuristicResult = cost.getTotalCost(currentSolution)
        if(solutions.lastOrNull()?.let { it.score <= heuristicResult } ?: false){
            //base case 1
            return null;
        }else if(notes.size==0){
            //base case 2
            storeSolution(heuristicResult,currentSolution)
            return currentSolution
        }else {
            //recursion
            val nextStates = positionFinder.findPositions(notes.first(), strings)
            val lowestCost = nextStates.sortedBy {
                currentSolution.lastOrNull()?.let {
                    previous -> cost.getAdditionalCost(previous,it)
                } ?: cost.getTotalCost(listOf(it))
            }
            val evaluatedAndValid = lowestCost.map { nextNotePosition -> //evaluate next notes
                generateStrategyCutoff(notes.drop(1), strings, currentSolution + nextNotePosition)
            }.filterNotNull()
            val bestSolution = evaluatedAndValid.map { nextNotePosition ->
                //calculate cost of each solution
                nextNotePosition to cost.getTotalCost(nextNotePosition)
            }.minBy { it.second }?.first
            return bestSolution
        }
    }

    private fun storeSolution(heuristicResult : Int, solution : List<PlayerState>){
        if(verbose)println("New best solution: $heuristicResult;")
        solutions += Solution(solution, heuristicResult)
        solutions = solutions.takeLast(solutionCount) //only keep n best results
    }
}