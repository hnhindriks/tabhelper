package search.strategies

import data.GuitarString
import data.Note
import misc.prettyPrinted
import search.PositionFinder
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction
import java.util.*

class RandomSearch(val cost : Cost = defaultCostFunction(), val positionFinder: PositionFinder = PositionFinder(), val timelimit : Int = 5000) : SearchAlgorithm {
    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>): List<Solution> {
        val start = System.currentTimeMillis()
        var bestResult: Solution? = null
        while(System.currentTimeMillis()-start < timelimit){
            val rand = Random()
            val newResult = notes.map { positionFinder.findPositions(it,strings).sortedBy { rand.nextInt() }.first() }
            val h = cost.getTotalCost(newResult);
            if(bestResult?.let { h < it.score} ?: true){
                println("New best result with score $h: ${newResult.prettyPrinted()}")
                bestResult = Solution(newResult,h)
            }
        }
        return listOf(bestResult!!)
    }
}


