package search.strategies

import data.GuitarString
import data.Note
import search.PlayerState
import search.PositionFinder
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction

data class GreedySearch(val cost : Cost = defaultCostFunction(), val positionFinder: PositionFinder = PositionFinder()) : SearchAlgorithm{
    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>): List<Solution>{
        val solution = notes.fold(listOf<PlayerState>(),{currentSolution, notegroup ->
            currentSolution + positionFinder.findPositions(notegroup,strings).minBy { cost.getAdditionalCost(currentSolution,it) }!!
        })
        //TODO do not recalculate cost
        return listOf(Solution(solution,cost.getTotalCost(solution)))
    }
}