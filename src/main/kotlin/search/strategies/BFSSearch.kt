package search.strategies

import data.GuitarString
import data.Note
import misc.prettyPrinted
import search.PlayerState
import search.PositionFinder
import search.SearchAlgorithm
import search.Solution
import search.costs.Cost
import search.costs.defaultCostFunction
import java.util.*

//Breadth-first-search algorithm, viable up to approx 5 levels of state depth
//TODO bugged
class BFSSearch(val cost : Cost = defaultCostFunction(), val positionFinder: PositionFinder = PositionFinder()) : SearchAlgorithm {
    override fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>) : List<Solution> {
        var queue : List<SearchState> = LinkedList()
        queue += SearchState(notes,listOf(),0)

        var bestResult : Solution? = null
        var maxLevel = -1;
        while(queue.isNotEmpty()){
            val curState = queue.first()
            if(curState.level>maxLevel){
                maxLevel = curState.level
                println("Deeper level: $maxLevel")
                println(positionFinder.localCache.stats())
            }
            if(curState.notesLeft.isEmpty()) {
                val h = cost.getTotalCost(curState.currentSolution)
                if(bestResult?.let { h < it.score } ?: false){
                    println("Storing solution with score $h: ${curState.currentSolution.prettyPrinted()}")
                    bestResult = Solution(curState.currentSolution,h)
                }
            }else{
                queue += (positionFinder.findPositions(curState.notesLeft.first(), strings).map { SearchState(curState.notesLeft.drop(1), curState.currentSolution + it,curState.level+1) })
            }
            queue = queue.drop(1)
        }
        return listOf(bestResult!!)
    }

    internal data class SearchState(val notesLeft: List<Set<Note>>, val currentSolution: List<PlayerState>, val level: Int)
}