package search

import data.GuitarString
import data.Note

interface SearchAlgorithm {
    /**
     * Generates a strategy for playing a given set of notes
     * @param notes the set of notes which should be analysed
     * *
     * @ensures \result.size()==notes.size()
     * *
     * @return a list with a player state for each note
     */
    fun generateStrategy(notes: List<Set<Note>>, strings: Collection<GuitarString>): List<Solution>
}

data class Solution(val theSolution : List<PlayerState>, val score : Int)