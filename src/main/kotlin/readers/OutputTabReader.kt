package readers

import data.GuitarString
import data.Note
import data.parseNote
import java.io.File
import java.util.regex.Pattern

class OutputTabReader : NotesReader{
    override fun readNotes(f: File): List<Set<Note>> {
        //Drop commented lines
        //Drop empty lines
        val filteredLines: List<String> = f.readLines().filter { !it.startsWith("#") }.filter { !it.isEmpty() }
        //Read the tuning
        val strings = filteredLines.first().split(regex = Pattern.compile("\\s")).map { parseNote(it) }.map { GuitarString(it) }

        //Read groups of |strings| lines
        var toProcess = filteredLines.drop(1)
        while(toProcess.size>=strings.size){
            val singleLineSet = toProcess.take(strings.size).map {
                it.split(" ").last()
            }
            toProcess = toProcess.drop(strings.size)
            singleLineSet.forEach { println(it) }
        }

        throw NotImplementedError()
    }
}