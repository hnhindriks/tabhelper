package readers

import data.Note
import java.io.File

interface NotesReader{
    fun readNotes(f: File): List<Set<Note>>
}
