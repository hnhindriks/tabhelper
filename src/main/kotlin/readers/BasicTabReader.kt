package readers

import data.GuitarString
import data.Note
import data.parseNote
import java.io.File
import java.util.regex.Pattern

class BasicTabReader : NotesReader {
    /**
     * Basic tab reader for simple 6-string guitar tabs, expects the following format:
     * On the first line, the tuning per string, from low to high:
     * E2 A3 D3 G3 B4 E4

     * Then afterwards a sequence of string-fret pairs:
     * 1;0 6;0 5;2 3;12 (E4,E2,B3,G4)

     * Lines starting with a # are ignored
     */
    override fun readNotes(f: File): List<Set<Note>> {
        val lines: List<String> = f.readLines().filter { !it.startsWith("#") }.filter { !it.isEmpty() }
        val strings = lines.first().split(regex = Pattern.compile("\\s")).map { parseNote(it) }.map { GuitarString(it) }
        return lines.drop(1).flatMap { it.split(regex = Pattern.compile("\\s")) }.map { it.split(delimiters = ";") }.map { parseSplitNote(it, strings) }.map { setOf(it) }
    }

    fun parseSplitNote(nd: List<String>, strings: List<GuitarString>): Note {
        val string = strings[strings.size - Integer.parseInt(nd[0])]
        return string.getNoteAtFret(Integer.parseInt(nd[1]))
    }
}