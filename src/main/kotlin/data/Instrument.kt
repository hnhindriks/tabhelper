package data

public val ESTANDARD = getSixStringTuning(Note(NoteName.E,2))

fun getSixStringTuning(base: Note) : List<GuitarString> = listOf(
        GuitarString(base),
        GuitarString(base.next(5)),
        GuitarString(base.next(10)),
        GuitarString(base.next(15)),
        GuitarString(base.next(19)),
        GuitarString(base.next(24))
)

fun getFiveStringTuning(base: Note) = listOf(
        GuitarString(base.next(5)),
        GuitarString(base.next(10)),
        GuitarString(base.next(15)),
        GuitarString(base.next(20)),
        GuitarString(base.next(25))
)

fun getFourStringTuning(base: Note) = listOf(
        GuitarString(base.next(5)),
        GuitarString(base.next(10)),
        GuitarString(base.next(15)),
        GuitarString(base.next(20))
)

data class GuitarString(val openNote: Note) {
    fun getNoteAtFret(fret: Int): Note {
        assert(canPlayNote(fret))
        return openNote.next(fret)
    }

    fun canPlayNote(n: Note): Boolean = canPlayNote(openNote.getDistance(n))

    fun canPlayNote(dist: Int): Boolean = dist >= 0 && dist <= 24

    fun getPosition(n: Note): Int? {
        val dist = openNote.getDistance(n)
        if (!canPlayNote(dist)) {
            return null
        } else {
            return dist
        }
    }

}

public data class StringPosition(val string: GuitarString, val position: Int = 0)