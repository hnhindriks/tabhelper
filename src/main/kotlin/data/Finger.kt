package data;

enum class Finger(val positionOffset:Int?) {OPEN(null), THUMB(null), INDEX(0), MIDDLE(1), RING(2), PINKY(3) }

data class FingerPosition(val finger: Finger, val position: Int = -1)
data class SingleNoteFingering(val string: GuitarString, val position: FingerPosition)