package data

enum class NoteName {
    A, AIS, B, C, CIS, D, DIS, E, F, FIS, G, GIS
}

data class Note(val noteName: NoteName, val octave: Int = 0) {

    //TODO: actual octave change starts from B,C in scientific pitch notation
    fun next(amount: Int = 1): Note {
        require(amount >= 0)
        val nextOrdinal = noteName.ordinal + amount
        return if (nextOrdinal < NoteName.values().size) {
            copy(noteName = NoteName.values()[nextOrdinal])
        } else {
            Note(
                    noteName = NoteName.values()[nextOrdinal % NoteName.values().size],
                    octave = octave + (nextOrdinal / NoteName.values().size)
            )
        }
    }

    /**
     * Returns the distance (in half-steps) between this and another note
     * @param n the note to calculate the distance against
     *
     * @return the distance in half-steps between this note and n
     */
    fun getDistance(n: Note): Int =
            12 * (n.octave - this.octave) + n.noteName.ordinal - this.noteName.ordinal

    fun getShortName(): String =
            noteName.toString() + octave
}

/**
 * Parses a single note of the format NoteName.octave
 * Flat and sharp notes are not yet supported
 * @param notestring a string containing the note
 *
 * @return The parsed note with silent status
 */
fun parseNote(notestring: String): Note {
    assert((notestring.length >= 2), {notestring})
    val notechar = notestring.toUpperCase().first().toString()
    val octave = Integer.parseInt(notestring.substring(1))
    return Note(NoteName.valueOf(notechar), octave)
}
