package misc

/**
 * The constants and methods in this file represent the stretch width of the hand of a guitar player
 * From experiments on my own hand, it seems that the relation can roughly be modelled by a linear equation.
 */

//Max and min frets for a 24-fret guitar
const val POSITION_MIN: Int = 0
const val POSITION_MAX: Int = 24

//Obtained by a single experiment with myself as the subject
const val STRETCH_BASE: Double = 3.1
const val STRETCH_FACTOR: Double = 1.25

//This resulted in a linear-like function which is modelled as follows:
fun maxFret(baseFret: Int) = Math.round((STRETCH_BASE + baseFret * STRETCH_FACTOR)).toInt().coerceIn(POSITION_MIN,POSITION_MAX)

//As the function is linear, we can invert it easily
fun minFret(baseFret: Int) = Math.round((baseFret-STRETCH_BASE)/STRETCH_FACTOR).toInt().coerceIn(POSITION_MIN,POSITION_MAX)

//Range of reachable positions from a certain fret
fun fretRange(baseFret: Int) = minFret(baseFret)..maxFret(baseFret)