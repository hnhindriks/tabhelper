package misc

import data.GuitarString
import search.PlayerState

fun <A> Iterable<A>.powerSet(): Set<Set<A>> {
    //base case
    var result = setOf(this.toSet(), setOf())

    //recursion
    for (element in this) {
        result += filter({ a -> a !== element }).powerSet()
    }
    return result
}

fun <S> Collection<S>.subSets(size: Int): Set<Set<S>> {
    if (size == 0) {
        return setOf()
    } else if (size == 1) {
        return this.map { setOf(it) }.toSet()
    } else {
        require(size <= this.size)

        var result = setOf<Set<S>>()

        //recursion
        for (element in this) {
            for (subset in this.filter { a -> a !== element }.subSets(size - 1)) {
                result = result.plus<Set<S>>(subset + element)
            }
        }
        return result
    }
}

fun List<PlayerState>.prettyPrinted() : String = this.map { "<" +
        "${it.position}," +
        "${it.fingering.map { "${it.key.openNote.getShortName()}:${it.value.finger}:${it.value.position}" }}" +
        ">" }.toString()

fun List<PlayerState>.toTabs(strings: List<GuitarString>) : String =
    strings.reversed().map { string -> "${string.openNote.noteName.name} " +
                this.map { it.fingering[string] }.map { it?.position?.toString() ?: "-"}.map { it.padStart(2,'-') }
                        .joinToString(separator="-") }
            .joinToString(separator = "\n")

/**
 * Prints the argument to stdout, and returns it, intended for use in expressions
 * @return a
 */
fun <A> printAndReturn(a : A) : A {
    println(a)
    return a;
}